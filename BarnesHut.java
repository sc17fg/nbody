import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import p1.Quad;
import p1.BHTree;
import p1.Body;

public class BarnesHut extends JPanel{
  public int N=100;
  public int M=100;
  public int InterceptVelocity=1000; // cant be to fast , want the galaxys to
  //rotate before they collide otherwise it wouldnt be a realsitic model
  //and then have have more interations between the particles
  //create an array list of bodies , dynamic size allows the program to scale
  public ArrayList<Body> bodies =new ArrayList<Body>();
  public double radius = 1e18;
  public static  double G= 6.673e-11;
  private static final int BOX_WIDTH = 1000;
  private static final int BOX_HEIGHT = 1000;
  private static final int UPDATE_RATE = 100;

  // create quad, 1000x bigger than the universe radius
  //as it is quarterd at each iteration , it only adds a couple more levels to
  //the tree and allows us to retain more accuracy
  public Quad q = new Quad(0,0,radius*1e3);
  /** Constructor to create the UI components and init game objects. */

  public BarnesHut() {
     this.setPreferredSize(new Dimension(BOX_WIDTH, BOX_HEIGHT));
     Thread simThread = new Thread() {
        public void run() {
           while (true) { // Execute one update step
             // Refresh the display
             repaint(); // Callback paintComponent()
             // Delay for timing control and give other threads a chance
             try {
                Thread.sleep(1000 / UPDATE_RATE);  // milliseconds
             } catch (InterruptedException ex) { }
          }
       }
    };
    simThread.start();  // Callback run() to start thread
 }

 @Override
   public void paintComponent (Graphics g){
       super.paintComponent(g);
       g.setColor(Color.BLACK);
       g.fillRect(0,0,BOX_WIDTH,BOX_HEIGHT);
       g.translate(BOX_WIDTH/2,BOX_HEIGHT/2);//this will need to change for final
       for(int i=0;i<(N+M);i++){
         //painting the bodies
         //im making the centers different colours and size from normal bodies for ease
         //display
         if(i==0 ){
           g.setColor(Color.red);
           g.fillOval((int) Math.round(bodies.get(i).rx*BOX_WIDTH/1e19),(int) Math.round(bodies.get(i).ry*BOX_HEIGHT/1e19),8,8);
         }
         else if(i==N){
           g.setColor(Color.green);
           g.fillOval((int) Math.round(bodies.get(i).rx*BOX_WIDTH/1e19),(int) Math.round(bodies.get(i).ry*BOX_HEIGHT/1e19),8,8);
         }
         else{
           g.setColor(Color.white);
           g.fillOval((int) Math.round(bodies.get(i).rx*BOX_WIDTH/1e19),(int) Math.round(bodies.get(i).ry*BOX_HEIGHT/1e19),3,3);
         }
       }
       addForces(N,M);
   }
//init the bodies
  public void init(){
    bodiesinit(N,M);
  }

  public static void main(String[] args) {
      javax.swing.SwingUtilities.invokeLater(new Runnable() {
         public void run() {
            BarnesHut Calc = new BarnesHut();
            Calc.init();
            JFrame frame = new JFrame("N-Body simulation with Barnes Hut approximations ");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setContentPane(Calc);
            frame.pack();
            frame.setVisible(true);
         }
      });
   }

   public void bodiesinit(int N, int M){
     double solarmass=1.9889e30;
     //first centre and then create the asssociated bodies
     bodies.add( new Body(-2e18,5e17,InterceptVelocity/2,0,4e6*solarmass));
     createBodies(N,bodies.get(0).rx,bodies.get(0).ry,bodies.get(0).vx,bodies.get(0).vy,bodies.get(0).mass);
     //second center and then create asssociated bodies
     bodies.add(new Body(2e18,-5e17,-InterceptVelocity/2,0,4e6*solarmass));
     createBodies(M,bodies.get(N).rx,bodies.get(N).ry,bodies.get(N).vx,bodies.get(N).vy,bodies.get(N).mass);
   }
   //this module will given the centers and all there positions and velocity calculate there relative positiona and velocity
   //and then calc the absolute velocity and position by adding the velocity and position of the center
   public void createBodies (int N, double crx , double cry, double cvx ,double cvy ,double cmass){
     double solarmass=1.9889e30;
     double radius=1e18;
     double offset=4e17;
     double G = 6.673e-11;
     double Direction = Math.random();// create a random direction for each galaxy to spin in
     if (Direction<=0.5)
       Direction = -1.0;

     else
       Direction = 1.0;
      //next generating the random points for the psuedo galaxy
     for(int i=1;i<N;i++){
       double r =(double)(Math.random()*radius)+offset;
       //offset required so that the objects are at least a set distance
    // from the center avoiding bodies to close to the center
       double angle = Math.random()*2*Math.PI; // random angle around the center
       double x = (double)(Math.cos(angle)*r)+crx; // calc x and y then
       double y = (double)(Math.sin(angle)*r)+cry;// addd the centers position to make
       // the pos absolute

       //mass with offset
       double mass = (double)((1+(Math.random())*5)*solarmass);
       double OrbitalVelocity = (double) Math.sqrt(G*(cmass)/r);// velocity calculated in terms of a circular orbit
       //change the velocity in to component form with trig
       double vx = Direction*OrbitalVelocity*Math.sin(angle);
       double vy = -Direction*OrbitalVelocity*Math.cos(angle);
       vx +=cvx; //add the horiozontal intercept to make the velocity absolute
       vy +=cvy; //add vert velocity
       //generate new body
       //add in this case to append to array list not the same methods as
       //adding center of mass pos
       bodies.add(new Body(x,y,vx,vy,mass));
     }
   }
  public void addForces(int N,int M){
    //the first step is to create the tree for each iteration
    BHTree Tree = new BHTree(q);
    //Insert all bodies into the tree
    for(int i=0;i<(N+M);i++){
      //check whether the object is still in the original quad
      //if it is then add it to the tree
      if(bodies.get(i).in(q)){
        Tree.insert(bodies.get(i));
      }
    }
    //now we calc the forces by recursively descending the tree
    for(int i=0;i<(N+M);i++){
      //reset the force for each timestep so that we are only working out forces
      //from that timestep
      bodies.get(i).resetForce();
      Tree.TreeForces(bodies.get(i));
    }
      //now we update the position of all particles.
      //Done in a sep loop so that its more acc
    for( int i=0 ; i<(N+M);i++){
      bodies.get(i).update(5.5e10);
    }
  }
}
