import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import p1.Body;

public class FullCalc extends JPanel{
  public int N= 100;
  public int M= 100;
  public int InterceptVelocity=1000; // cant be to fast , want the galaxys to rotate before they collide and then have have more interations between the particles
  //create an array list of bodies , dynamic size allows the program to scale
  public ArrayList<Body> bodies =new ArrayList<Body>();
  public static  double G= 6.673e-11;
  private static final int BOX_WIDTH = 1000;
  private static final int BOX_HEIGHT = 1000;
  private static final int UPDATE_RATE = 100;
  /** Constructor to create the UI components and init game objects. */
  public FullCalc() {
     this.setPreferredSize(new Dimension(BOX_WIDTH, BOX_HEIGHT));
     Thread simThread = new Thread() {
        public void run() {
           while (true) { // Execute one update step
             // Refresh the display
             repaint(); // Callback paintComponent()
             // Delay for timing control and give other threads a chance
             try {
                Thread.sleep(1000 / UPDATE_RATE);  // milliseconds
             } catch (InterruptedException ex) { }
          }
       }
    };
    simThread.start();  // Callback run()
 }

@Override
  public void paintComponent (Graphics g){
      super.paintComponent(g);
      g.setColor(Color.BLACK);
      g.fillRect(0,0,BOX_WIDTH,BOX_HEIGHT);
      g.translate(BOX_WIDTH/2,BOX_HEIGHT/2);//this will need to change for final
      for(int i=0;i<(N+M);i++){
        if(i==0){
          g.setColor(Color.red);
          g.fillOval((int) Math.round(bodies.get(i).rx*BOX_WIDTH/1e19),(int) Math.round(bodies.get(i).ry*BOX_HEIGHT/1e19),8,8);
        }
      else  if(i==N){
          g.setColor(Color.green);
          g.fillOval((int) Math.round(bodies.get(i).rx*BOX_WIDTH/1e19),(int) Math.round(bodies.get(i).ry*BOX_HEIGHT/1e19),8,8);
        }
        else{
          g.setColor(Color.white);
          g.fillOval((int) Math.round(bodies.get(i).rx*BOX_WIDTH/1e19),(int) Math.round(bodies.get(i).ry*BOX_HEIGHT/1e19),3,3);
        }
      }
      addForces(N,M);
  }

  public void init(){
    bodiesinit(N,M);
  }

  public static void main(String[] args) {
      javax.swing.SwingUtilities.invokeLater(new Runnable() {
         public void run() {
            FullCalc Calc = new FullCalc();
            Calc.init();
            JFrame frame = new JFrame("N-Body simulation");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setContentPane(Calc);
            frame.pack();
            frame.setVisible(true);
         }
      });
   }

   public void bodiesinit(int N, int M){
     double solarmass=1.9889e30;
     //first centre and then create the asssociated bodies
     bodies.add( new Body(-2e18,5e17,InterceptVelocity/2,0,4e6*solarmass));
     createBodies(N,bodies.get(0).rx,bodies.get(0).ry,bodies.get(0).vx,bodies.get(0).vy,bodies.get(0).mass);
     //second center and then create asssociated bodies
     bodies.add(new Body(2e18,-5e17,-InterceptVelocity/2,0,4e6*solarmass));
     createBodies(M,bodies.get(N).rx,bodies.get(N).ry,bodies.get(N).vx,bodies.get(N).vy,bodies.get(N).mass);
   }
  //this module will given the centers and all there positions and velocity calculate there relative positiona and velocity
  //and then calc the absolute velocity and position by adding the velocity and position of the center itself to it
  public void createBodies (int N, double crx , double cry, double cvx ,double cvy ,double cmass){
    double solarmass=1.9889e30;
    double radius=1e18;
    double offset=4e17;
    double G = 6.673e-11;
    double Direction = Math.random();// create a random direction for each galaxy to spin in
    if (Direction<=0.5)
      Direction = -1.0;

    else
      Direction = 1.0;

    for(int i=1;i<N;i++){
      double r =(double)(Math.random()*radius)+offset;//offset required so that
    // the objects are at least a set distance from the center avoiding
    // really fast stars close to the center
      double angle = Math.random()*2*Math.PI; // random angle around the center
      double x = (double)(Math.cos(angle)*r)+crx;
      double y = (double)(Math.sin(angle)*r)+cry;
      //mass with offset
      double mass = (double)((1+(Math.random())*5)*solarmass);
      double OrbitalVelocity = (double) Math.sqrt(G*(cmass)/r);// velocity calculated in terms of a circular orbit
      double vx = Direction*OrbitalVelocity*Math.sin(angle); //change the velocity in to component form by
      double vy = -Direction*OrbitalVelocity*Math.cos(angle);
      vx +=cvx; //add the horiozontal intercept to make the velocity absolute
      vy +=cvy; //add verticle velocity
      bodies.add(new Body(x,y,vx,vy,mass));
    }
  }
  public void addForces(int N,int M){
    for(int i=0;i<(N+M);i++){
      bodies.get(i).resetForce();
      for (int j=0;j<(N+M);j++){
        if (i != j){
          //implement force pairing to half the number of calc
          bodies.get(i).AddForce(bodies.get(j));
        }
      }
    }
    for(int i=0;i<(N+M);i++){
      bodies.get(i).update(5.5e10);
    }
  }
}
