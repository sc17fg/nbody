package p1;
public class BHTree {
  private Body body;     // body or aggregate body stored in this node
  private Quad quad;     // square region that the tree represents
  private BHTree NW;     // tree representing northwest quadrant
  private BHTree NE;     // tree representing northeast quadrant
  private BHTree SW;     // tree representing southwest quadrant
  private BHTree SE;     // tree representing southeast quadrant
  private double theta = 0.5;	//Theta is used for calculating interactions

  //constructor for BH tree
  //intialise the tree with given params

  public BHTree(Quad q) {
    this.quad=q;
    this.body=null;
    this.NW=null;
    this.NE=null;
    this.SW=null;
    this.SE=null;
  }

  // method to check whether the node is a leaf of tree ( external ) ( no children nodes)
  public Boolean isExternal() {
    return (this.NW==null && this.NE==null && this.SW==null && this.SE==null);
  }

    ///////*********************
  //Adding a body to the Quad-tree
  public void insert(Body b ){
    //The first case is the node we are at is empty
    // in this case we can just insert the body
    if ( this.body == null ){
      this.body = b;
      return;
    }
    //The next case is when the body is internal , we should insert it
    // then recursively find where it goes in the sub quads
    else if (isExternal() == false ){
      //update center
      this.body = this.body.add(this.body,b);
      //find the sub quad the body is in and insert it
      SubBody(b);

    }
    else if (isExternal()){
      // Third case is when we are at an external node
      //First lets create 4 new Sub quads for that node
      this.NW = new BHTree(quad.NW());
      this.SE = new BHTree(quad.SE());
      this.NE = new BHTree(quad.NE());
      this.SW = new BHTree(quad.SW());

      // Insert the body already in the node and the new body into
      //there given sub quads
      SubBody(this.body);
      SubBody(b);
      //create the center of (mass and pos)
      this.body=this.body.add(this.body,b);
    }
  }
  //private method to work out which sub quad to insert ,
  //private as only needs to be accessed within the class
  //this is the recursive part of the tree insertion

  private void SubBody(Body b) {
        if (b.in(quad.NW()))
            this.NW.insert(b);
        else if (b.in(quad.NE()))
            this.NE.insert(b);
        else if (b.in(quad.SE()))
            this.SE.insert(b);
        else if (b.in(quad.SW()))
            this.SW.insert(b);
    }

  ///////*********************
  //Next method is to calculate the force by decending the populated tree and comparing distance to theta
  public void TreeForces (Body b){
    if (this.body==null || b == this.body){
      //try and avoid unneccessary computations
      //therefore we should stop if the body is null or if the
      //tree is trying to work out force between the same body
      return;
    }
    else if (isExternal()){
  //case 1 we are on an external node , then we can use newtons equations
  // as there is no cluster to approx
      b.AddForce(this.body);
    }
    else {
      // now we consider the cases of internal nodes and the approximations
      //that can be calculated from the tree
      double Quad_Length = quad.length();
      double distance_to_centerofmass = b.Distance(this.body);
      //the next step is to calc the ratio between the length of the quad and
      //the distance between the body and the cluster
      //we  compare it to theta , if it is far enough away we approx , otherwise
      //we recurse down the tree
      if(( Quad_Length / distance_to_centerofmass ) < theta ){
        //Approximate with pseudo body that is a center of mass
        b.AddForce(this.body);
      }
      else {
        //recurse down the tree as we are too close to approx
        this.NW.TreeForces(b);
        this.NE.TreeForces(b);
        this.SE.TreeForces(b);
        this.SW.TreeForces(b);
      }
    }
  }
}
