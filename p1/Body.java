package p1;
import java.awt.Color;
public class Body {
  private static final double G=6.673e-11;
  private static final double solarmass=1.9889e30;
  //constants i will use with each body

  //Next section holds information on the bodies
  public double rx, ry;//position vector
  public double vx, vy;//velocity vector
  public double fx, fy;//force vectors
  public double mass;

  //create and intiatalise a body
  public Body(double rx, double ry,double vx, double vy,double mass){
    this.rx=rx;
    this.ry=ry;
    this.vx=vx;
    this.vy=vy;
    this.mass=mass;
  }
  //method to work out distance between two objects
  public double Distance(Body a){
    double dx=a.rx-this.rx;
    double dy=a.ry-this.ry;
    return Math.sqrt(dx*dx+dy*dy);
  }
  public void resetForce(){
    this.fx=0.0;
    this.fy=0.0;
  }
  public void AddForce (Body A){
    double Soften=3E4;//3E4
    double dy=A.ry-this.ry;
    double dx=A.rx-this.rx;
    double Distance=this.Distance(A);
    //use soften to avoid dividing by zero
    double Force=(G*A.mass*this.mass)/(Distance*Distance+Soften*Soften);
    // split the force into x and y componets
    this.fx+=Force*dx/Distance;
    this.fy+=Force*dy/Distance;
  }

  public String toString(){
    return "x" + rx + ", y"+ ry + ",vx "+  vx+ ", vy"+ vy+ ", m"+ mass;
  }

  public void update(double dt) {
  vx += dt * fx / mass;
  vy += dt * fy / mass;
  rx += dt * vx;
  ry += dt * vy;
  }

//New Methods for Barnes-Hut
//see if a bodys is withing a given quad
  public Boolean in(Quad q){
    return q.contains(this.rx,this.ry);
  }

//this method updates the center of mass and pos
//this part doesnt need to store the velocity as we are just
  public Body add(Body a , Body b){
    double mass_total = a.mass + b.mass;
    Body average = new Body((a.rx*a.mass+b.rx*b.mass)/(mass_total),(a.ry*a.mass+b.ry*b.mass)/(mass_total),0.0,0.0,mass_total);
    return average;
  }
}
