package p1;
public class Quad{
  // x,y represent midpoints of the repective axis so ((x,y) is center of quad)
  //we want to create a square quad that can be recursively subsivided into 4 equal parts
  private double x;
  private double y;
  private double length;

  public Quad( double x, double y, double length){
    this.x=x;
    this.y=y;
    this.length=length;
  }
  //getter for length
  public double length (){
    return length;
  }

  // check whether a quad contains a given point

  public boolean contains(double rx, double ry) {

      double halfLen = this.length / 2.0;
      // four conditions that must be met for a body to be in a quad
      // checking all edges of the quad
      return (rx <= this.x + halfLen && rx >= this.x - halfLen &&
              ry <= this.y + halfLen && ry >= this.y - halfLen ) ;
  }

//Methods to create the 4 diff sub quads.

  public Quad NW (){
    return new Quad(this.x-this.length/4.0, this.y+this.length/4.0, this.length/2.0);

  }
  public Quad NE (){
    return new Quad(this.x+this.length/4.0, this.y+this.length/4.0, this.length/2.0);
  }
  public Quad SE (){
    return new Quad(this.x+this.length/4.0, this.y-this.length/4.0, this.length/2.0);

  }
  public Quad SW (){
    return new Quad(this.x-this.length/4.0, this.y-this.length/4.0, this.length/2.0);
  }



}
